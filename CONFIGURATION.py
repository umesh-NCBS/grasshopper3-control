N_FRAMES = float('inf')
#N_FRAMES = 100
FPS = 60
PREVIEW_EVERY = 10
TEENSY_PORT = "/dev/ttyACM0"
CIRCULAR_BUFFER_N_FRAMES = 500

'''
CONFIGURATION = {
    18275010: {'output': '{timestamp}_{serial}.hdf5',
               'settings': {'brightness': 0,
                            'exposure': 0,
                            'frame_rate': FPS,
                            'gain': 25,
                            'gamma': 1,
                            'shutter': 10,
                            'is master': True,
                            'roi': (0, 0, 2448, 1800)}}
}
# '''
CONFIGURATION = {
#    18275010: {'output': '{timestamp}_{serial}.hdf5',
#               'settings': {'brightness': 0,
#                            'exposure': 0,
#                            'frame_rate': FPS,
#                            'gain': 25,
#                            'gamma': 1,
#                            'shutter': 10,
#                            'is master': False,
#                            'roi': (0, 0, 2448, 1800)}},
    18275022: {'output': '{timestamp}_{serial}.hdf5',
               'settings': {'brightness': 0,
                            'exposure': 0,
                            'frame_rate': FPS,
                            'gain': 25,
                            'gamma': 1,
                            'shutter': 10,
                            'is master': False,
                            'roi': (0, 250, 2448, 1250)}},
    18275005: {'output': '{timestamp}_{serial}.hdf5',
               'settings': {'brightness': 0,
                            'exposure': 0,
                            'frame_rate': FPS,
                            'gain': 25,
                            'gamma': 1,
                            'shutter': 10,
                            'is master': False,
                            'roi': (0, 250, 2448, 1250)}}
}
# '''
