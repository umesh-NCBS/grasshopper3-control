First working multi camera (all slave) synchronized (external strobe signal) 
Grasshopper camera recording with preview during recording.

Documentation, source comments and usage examples to follow in later releases

# Short description of setup
Hardware:
*  PCIEx4 to quad USB3 card (dedicated buses and not shared bus)
*  NVMe storage (or equivalent speed raid configuration) to write data into
*  At least 2 virtual cores per camera + 1 virtual core for other computation
A 4 core (8 hyperthread) CPU along with Samsung NVMe 960 Pro and about 16 GB is 
pretty good for 3 camera continuous streaming to disk at full resolution and 
maximum framerate indefinitely (till the NVMe fills up) (Circular data file to 
be implemented in a later release)

Install the following:
*  flycap2 SDK from FLIR
*  PyCapture2 for python from FLIR
*  ffmpeg
*  python 3.6/3.7 with h5py, numpy, pyqtgraph, pyqt5, loguru, argos (to view 
hdf5)

# Usage
Run flycap2 and figure out the roi, shutter, gain, gamma, brightness, exposure 
in that order for the chosen frame rate for each camera and note it in the 
CONFIGURATION.py. Also mention how many frames you want to capture (per camera) 
in N_FRAMES and how often you want to see the preview image. At time of testing,
an effective preview frame rate of 10 fps works. So while recording at 60 fps, 
set PREVIEW_EVERY to 6 for best results. Press backspace at any time during 
recording to interrupt and stop the recordings. The last few frames from the 
cameras might not be retrieved in this case.