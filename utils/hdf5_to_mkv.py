#! /usr/bin/env python3

# Author: Umesh Mohan (moh@nume.sh)
'''HDF5 to x265 mkv video

Usage:
  hdf5_to_mkv.py <input_hdf5_file> <experiment> <moth_id> [options]
  hdf5_to_mkv.py (-h | --help)
  hdf5_to_mkv.py (-v | --version)

Options:
  -h --help                     Show this screen
  -v --version                  Show version
  -d --date=<date>              Date [default: from_file_name]
  -o --output=<output_file>     Output mkv file [default: input_file_with_mkv_extension]
  -f --framerate=<framerate>    Set output framerate [default: 20]
  --crf=<crf>                   Set output crf (Constant Rate Factor: 0-51, 0 is lossless, 51 is worst quality, ~17 is visually lossless) [default: 0]

'''


import argparse
from subprocess import Popen, PIPE
import h5py
import os


def ffmpeg_write_pipe(file_path, width, height, fps, crf):
    if crf == 0:
        crf_param = '-x265-params'
        crf_param_value = 'lossless=1'
    else:
        crf_param = '-crf'
        crf_param_value = str(crf)
    ffmpeg_path = 'ffmpeg'
    ffmpeg_command = [ffmpeg_path, '-y',
                      '-r', str(fps),
                      '-f', 'rawvideo',
                      '-vcodec', 'rawvideo',
                      '-s', str(width)+'x'+str(height),
                      '-pix_fmt', 'gray',
                      '-i', '-',
                      '-vcodec', 'libx265',
                      '-preset', 'ultrafast',
                      crf_param, crf_param_value,
                      '-pix_fmt', 'yuv420p',
                      '-an',
                      '-threads', '0',
                      file_path]
    return Popen(ffmpeg_command, stdin=PIPE)


def hdf5_to_mp4(hdf5_file=None, output_file=None, fps=20, crf=20):
    if output_file is None:
        output_file = hdf5_file[:-4] + 'mp4'
    data_hdf5 = h5py.File(hdf5_file, 'r')
    (height, width) = data_hdf5[list(data_hdf5.keys())[0]].value.shape
    ffmpeg_pipe = ffmpeg_write_pipe(output_file, width, height, fps, crf)
    frame_numbers = list(data_hdf5.keys())
    for frame_number in frame_numbers[1:]:
        frame = data_hdf5[frame_number]
        ffmpeg_pipe.stdin.write(frame.value[::-1, ::1].tobytes())
    ffmpeg_pipe.terminate()


def hdf5_to_srt(hdf5_file, output_file=None, fps=20,
                experiment=None, moth_id=None, date="from_file_name"):
    if output_file is None:
        output_file = hdf5_file[:-4] + 'srt'
    data_hdf5 = h5py.File(hdf5_file, 'r')
    frame_numbers = list(data_hdf5.keys())
    n_frames = len(frame_numbers) - 1
    t0 = data_hdf5[frame_numbers[1]].attrs['timestamp']
    info = ''
    for attribute_name, value in data_hdf5.attrs.items():
        info += '{}: {}\n'.format(attribute_name, value)
    if 'circular buffer n frames' not in data_hdf5.attrs:
        info += 'circular buffer n frames: 500'
    with open(output_file, 'w') as srt_file:
        i = 1
        srt_file.write('\n{}\n00:00:00.000 --> 00:00:00.001\n{}\n'.format(i, info))
        i += 1
        for frame_number in frame_numbers[1:]:
            t = data_hdf5[frame_number].attrs['timestamp'] - t0


if __name__=='__main__':
    from docopt import docopt
    arguments = docopt(__doc__, version='HDF5 to x265 mkv video 0.1')
    hdf5_file = arguments['<input_hdf5_file>']
    output_file = arguments['--output']
    experiment = arguments['<experiment>']
    moth_id = arguments['<moth_id>']
    date = arguments['--date']
    if output_file == 'input_file_with_mkv_extension':
        output_file = hdf5_file[:-4] + 'mkv'
    if date == 'from_file_name':
        date = hdf5_file.split('_')[0]
    hdf5_to_mp4(hdf5_file, output_file=output_file,
                fps=float(arguments['--framerate']),
                crf=int(arguments['--crf']))
    hdf5_to_srt(hdf5_file, output_file=output_file,
                fps=float(arguments['--framerate']),
                experiment=experiment,
                moth_id=moth_id,
                date=date)
