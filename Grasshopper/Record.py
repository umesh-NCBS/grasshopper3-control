# Author: Umesh Mohan (moh@nume.sh)

from loguru import logger
import multiprocessing as mp
from .FrameCapture import frame_capture
from .Write2HDF5 import frame_save
from .Interrupt import disable_keyboard_interrupt,\
                       enable_keyboard_interrupt,\
                       recording_interrupt


def record(configuration, n_frames, preview_nth_frame=10, preview_queue=None,
           circular_buffer_n_frames=500,
           interrupt_event=None, stop_circular_write_event=None):
    '''
    configuration = {
        camera_serial_number: {'output': output_hdf5_file,
                            'preview pipe source': preview_pipe_source,
                            'preview pipe sink': preview_pipe_sink,
                            'preview queue': preview_queue,
                            'settings': camera_settings}
                    }
    '''
    if interrupt_event is None:
        interrupt_event = mp.Event()
        external_interrupt = False
    else:
        external_interrupt = True
    master_recording_done_event = mp.Event()
    master = []
    slaves = []
    for camera_serial_number in configuration.keys():
        if configuration[camera_serial_number]['settings']['is master']:
            master.append(camera_serial_number)
        else:
            slaves.append(camera_serial_number)
    capture_processes = []
    save_processes = []
    recording_finished_events = []
    saving_finished_events = []
    data_pipes_source = []
    data_pipes_sink = []
    for camera_serial_number in slaves + master:
        data_pipe_sink, data_pipe_source = mp.Pipe(False)
        data_pipes_source.append(data_pipe_source)
        data_pipes_sink.append(data_pipe_sink)
        recording_finished_event = mp.Event()
        recording_finished_events.append(recording_finished_event)
        saving_finished_event = mp.Event()
        saving_finished_events.append(saving_finished_event)
        camera_settings = configuration[camera_serial_number]['settings']
        capture_processes.append(mp.Process(
            target=frame_capture,
            args=(camera_serial_number, n_frames, 
                  data_pipe_source, data_pipe_sink),
            kwargs={'interrupt_event': interrupt_event,
                    'recording_finished_event': recording_finished_event,
                    'master_recording_done_event': master_recording_done_event,
                    'camera_settings': camera_settings}))
        hdf5_file = configuration[camera_serial_number]['output']
        save_processes.append(mp.Process(
            target=frame_save,
            args=(data_pipe_sink,),
            kwargs={'file_path': hdf5_file,
                    'saving_finished_event': saving_finished_event,
                    'stop_circular_write_event': stop_circular_write_event,
                    'circular_buffer_n_frames': circular_buffer_n_frames,
                    'camera_settings': camera_settings,
                    'preview_queue': preview_queue,
                    'preview_nth_frame': preview_nth_frame,
                    'camera_serial_number': camera_serial_number}))
    if not external_interrupt:
        recording_interrupt_process = mp.Process(
            target=recording_interrupt,
            args=(interrupt_event,
                  recording_finished_events,
                  saving_finished_events))
    original_sigint_handler = disable_keyboard_interrupt()
    for capture_process in capture_processes:
        capture_process.start()
        logger.info('Capture pid: {}'.format(capture_process.pid))
    for data_pipe_source in data_pipes_source:
        data_pipe_source.close()
    for save_process in save_processes:
        save_process.start()
        logger.info('Save pid: {}'.format(save_process.pid))
    for data_pipe_sink in data_pipes_sink:
        data_pipe_sink.close()
    enable_keyboard_interrupt(original_sigint_handler)
    if not external_interrupt:
        recording_interrupt_process.start()
        logger.info('Recording Interrupt pid: {}'.format(
            recording_interrupt_process.pid))
    original_sigint_handler = disable_keyboard_interrupt()
    logger.trace('Waiting for processes to end')
    for capture_process in capture_processes:
        capture_process.join()
    logger.trace('Capture processes has exited')
    for save_process in save_processes:
        save_process.join()
    logger.trace('Save processes has exited')
    if not external_interrupt:
        recording_interrupt_process.join()
        logger.trace('Keyboard interrupt process has exited')
    enable_keyboard_interrupt(original_sigint_handler)
    logger.trace('Record process done')
