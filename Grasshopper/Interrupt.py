# Author: Umesh Mohan (moh@nume.sh)

from loguru import logger
from time import sleep
import signal


def recording_interrupt(interrupt_event,
                        recording_finished_events,
                        saving_finished_events):
    logger.trace('Recording interrupt process started')
    while True:
        try:
            sleep(0.1)
            pending = False
            for recording_finished_event in recording_finished_events:
                if not recording_finished_event.is_set():
                    pending = True
            for saving_finished_event in saving_finished_events:
                if not saving_finished_event.is_set():
                    pending = True
            if not pending:
                logger.info('Recording and Saving finished. Exiting')
                return
        except KeyboardInterrupt:
            interrupt_event.set()
            logger.info('Caught "ctrl+c". Trying to exit gracefully')


def disable_keyboard_interrupt():
    original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
    return original_sigint_handler


def enable_keyboard_interrupt(original_sigint_handler):
    signal.signal(signal.SIGINT, original_sigint_handler)
