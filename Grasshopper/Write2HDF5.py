# Author: Umesh Mohan (moh@nume.sh)

import h5py
import numpy as np
from loguru import logger
from datetime import datetime


def timestamp():
    now = datetime.now()
    return '{:04d}-{:02d}-{:02d}_{:02d}.{:02d}.{:02d}'\
        .format(now.year, now.month, now.day, 
                now.hour, now.minute, now.second)


def frame_save(data_pipe_sink, file_path='temp.hdf5',
               saving_finished_event=None, camera_settings={},
               preview_queue=None, preview_nth_frame=10,
               circular_buffer_n_frames=500, stop_circular_write_event=None,
               camera_serial_number=None):
    file_path = file_path.replace('{serial}', str(camera_serial_number))
    file_path = file_path.replace('{timestamp}', timestamp())
    logger.debug('Frame save process started. Saving to: {}'.format(file_path))
    data_file = h5py.File(file_path, 'w')
    for camera_setting, value in camera_settings.items():
        data_file.attrs['Camera Setting: ' + camera_setting] = value
    data_file.attrs['Camera serial number'] = camera_serial_number
    data_file.attrs['circular buffer n frames'] = circular_buffer_n_frames
    while True:
        try:
            [frame_number, image] = data_pipe_sink.recv()
            image_data = np.reshape(image['data'], image['dimension']),
            frame_number_string = '{:06d}'.format(frame_number)
            data_file.create_dataset(name=frame_number_string,
                                     data=image_data)
            logger.trace('Wrote frame: {}'.format(frame_number_string))
            if stop_circular_write_event is not None:
                if not stop_circular_write_event.is_set():
                    if frame_number > circular_buffer_n_frames:
                        frame_number_to_delete = frame_number - circular_buffer_n_frames
                        frame_number_to_delete_string = '{:06d}'.format(frame_number_to_delete)
                        del data_file[frame_number_to_delete_string]
                        logger.trace('Deleted frame: {}'.format(frame_number_to_delete_string))
            data_file[frame_number_string].attrs['timestamp'] = \
                image['timestamp']
            if preview_queue is not None:
                if frame_number % preview_nth_frame == 0:
                    preview_queue.put([frame_number, image])
        except EOFError:
            logger.debug('data pipe sink is closed')
            break
    #if preview_queue is not None:
    #    if frame_number % preview_nth_frame == 0:
    #        preview_queue.put([frame_number, image])
    data_file.close()
    data_pipe_sink.close()
    saving_finished_event.set()
    logger.debug('Frame save process done')
