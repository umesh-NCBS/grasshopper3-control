# Author: Umesh Mohan (moh@nume.sh)

import numpy as np
from loguru import logger
from time import sleep


def frame_capture(camera_serial_number, n_frames,
                  data_pipe_source, data_pipe_sink,
                  interrupt_event=None, recording_finished_event=None,
                  master_recording_done_event=None,
                  camera_settings={}):
    logger.debug('Frame capture process started. Initializing camera...')
    logger.trace(camera_settings)
    data_pipe_sink.close()
    from .Camera import Camera, pycap, cameras_available
    if camera_serial_number in cameras_available:
        camera = Camera(camera_serial_number, 
                        is_master=camera_settings['is master'])
        for camera_setting, value in camera_settings.items():
            if camera_setting != 'is master':
                setattr(camera, camera_setting, value)
        frame_number = 0
        if camera_settings['is master']:
            sleep(2)  # Wait for all other processes to start before
            # initiating master record
        camera.start_capture()
        while frame_number < n_frames:
            try:
                image = camera.get_image()
                image_dict = {'data': image.getData(),
                              'timestamp': image.getTimeStamp().seconds +
                                 (image.getTimeStamp().microSeconds / 1000000),
                              'camera serial number': camera_serial_number,
                              'dimension': (image.getRows(), image.getCols())}
                data_pipe_source.send([frame_number, image_dict])
                frame_number += 1
            except pycap.Fc2error:
                #sleep(0.014)
                if not camera_settings['is master']:
                    if master_recording_done_event.is_set():
                        break
            if interrupt_event is not None:
                if interrupt_event.is_set():
                    break
            '''logger.trace('Camera {}: Frames recorded: {}, \
Interrupt is set: {}, Master is done: {}'.format(
                    camera_serial_number, frame_number,
                    interrupt_event.is_set(),
                    master_recording_done_event.is_set()))'''
        camera.stop_capture()
        while True:
            try:
                image = camera.get_image()
                data_pipe_source.send([frame_number, image])
                frame_number += 1
            except pycap.Fc2error:
                break
        camera.disconnect()
    else:
        logger.error('Camera with serial number {} is not available'.format(
            camera_serial_number))
    data_pipe_source.close()
    if recording_finished_event is not None:
        recording_finished_event.set()
    if camera_settings['is master']:
        if master_recording_done_event is not None:
            master_recording_done_event.set()
    logger.debug('Frame capture process done')
