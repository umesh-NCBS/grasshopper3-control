# Author: Umesh Mohan (moh@nume.sh)

import PyCapture2 as pycap
from loguru import logger

BUS_MANAGER = pycap.BusManager()
PROPERTY = {'brightness': pycap.PROPERTY_TYPE.BRIGHTNESS,
            'exposure': pycap.PROPERTY_TYPE.AUTO_EXPOSURE,
            'gamma': pycap.PROPERTY_TYPE.GAMMA,
            'shutter': pycap.PROPERTY_TYPE.SHUTTER,
            'gain': pycap.PROPERTY_TYPE.GAIN,
            'frame rate': pycap.PROPERTY_TYPE.FRAME_RATE}


def list_cameras():
    cameras = []
    camera = pycap.Camera()
    for camera_index in range(BUS_MANAGER.getNumOfCameras()):
        camera.connect(BUS_MANAGER.getCameraFromIndex(camera_index))
        camera_info = camera.getCameraInfo()
        cameras.append(camera_info.serialNumber)
        camera.disconnect()
    return cameras


cameras_available = list_cameras()


class Camera(object):
    def __init__(self, camera_serial_number, is_master=True):
        self._camera = pycap.Camera()
        if camera_serial_number in cameras_available:
            self._camera.connect(BUS_MANAGER.getCameraFromSerialNumber(
                camera_serial_number))
            logger.success('Camera {} connected'.format(camera_serial_number))
        self.serial_number = camera_serial_number
        self.is_master = is_master
        self.reset()
        self.state = 'Connected'

    def get_property(self, property_):
        return self._camera.getProperty(PROPERTY[property_]).absValue

    def set_property(self, property_, value):
        property_info = self._camera.getPropertyInfo(PROPERTY[property_])
        assert property_info.absMin <= value <= property_info.absMax
        self._camera.setProperty(type=PROPERTY[property_],
                                 absValue=value, autoManualMode=False)
        actual_value = self.get_property(property_)
        logger.success('Camera {}\'s {} has been set to {} {}'.format(
            self.serial_number, property_, actual_value, 
            property_info.unitAbbr.decode('UTF-8')))
        return actual_value

    @property
    def roi(self):
        format7_image_setting, packet_size, packet_percent = \
            self._camera.getFormat7Configuration()
        return (format7_image_setting.offsetX, format7_image_setting.offsetY,
                format7_image_setting.width, format7_image_setting.height)

    @roi.setter
    def roi(self, value):
        offset_x, offset_y, width, height = value
        assert offset_x % self.x_step == 0
        assert offset_y % self.y_step == 0
        assert width % self.width_step == 0
        assert height % self.height_step == 0
        assert offset_x + width <= self.max_width
        assert offset_y + height <= self.max_height
        format7_image_setting, packet_size, packet_percent = \
            self._camera.getFormat7Configuration()
        format7_image_setting.offsetX = offset_x
        format7_image_setting.offsetY = offset_y
        format7_image_setting.width = width
        format7_image_setting.height = height
        self._camera.setFormat7Configuration(packet_percent, 
                                             format7_image_setting)
        format7_image_setting, packet_size, packet_percent = \
            self._camera.getFormat7Configuration()
        logger.success('Camera {}\'s ROI has been set to ({},{}) {}x{}'.format(
            self.serial_number,
            format7_image_setting.offsetX, format7_image_setting.offsetY,
            format7_image_setting.width, format7_image_setting.height))

    @property
    def gain(self):
        return self.get_property('gain')

    @gain.setter
    def gain(self, value):
        self.set_property('gain', value)

    @property
    def shutter(self):
        return self.get_property('shutter')

    @shutter.setter
    def shutter(self, value):
        self.set_property('shutter', value)

    @property
    def brightness(self):
        return self.get_property('brightness')

    @brightness.setter
    def brightness(self, value):
        self.set_property('brightness', value)

    @property
    def gamma(self):
        return self.get_property('gamma')

    @gamma.setter
    def gamma(self, value):
        self.set_property('gamma', value)

    @property
    def exposure(self):
        return self.get_property('exposure')

    @exposure.setter
    def exposure(self, value):
        self.set_property('exposure', value)

    @property
    def frame_rate(self):
        return self.get_property('frame rate')

    @frame_rate.setter
    def frame_rate(self, value):
        if self.is_master:
            self.set_property('frame rate', value)
        else:
            self.set_property('frame rate', 1)

    @property
    def strobe_out_pin(self):
        return self._strobe_out_pin

    @strobe_out_pin.setter
    def strobe_out_pin(self, value):
        assert value in [None, 1, 2, 3]
        self._strobe_out_pin = value
        for pin_number in [1, 2, 3]:
            self._camera.setStrobe(onOff=False,  # Disable strobe out
                                   polarity=0,  # High->Low
                                   source=pin_number,
                                   delay=0, duration=0)
        if value in [1, 2, 3]:
            self._camera.setStrobe(onOff=True,  # Disable strobe out
                                   polarity=0,  # High->Low
                                   source=value,
                                   delay=0, duration=0)
        logger.trace('Strobe out pin set to ' + str(value))

    @property
    def strobe_in_pin(self):
        return self._strobe_in_pin

    @strobe_in_pin.setter
    def strobe_in_pin(self, value):
        assert value in [None, 0, 2, 3]
        self._strobe_in_pin = value
        if value is None:
            self._camera.setTriggerMode(onOff=False,  # Interal trigger
                                        polarity=0,  # High->Low
                                        source=4,  # None
                                        mode=0,
                                        parameter=0)
        elif value in [0, 2, 3]:
            self._camera.setTriggerMode(onOff=True,  # External trigger
                                        polarity=0,  # High->Low
                                        source=value,
                                        mode=14,
                                        parameter=0)
        logger.trace('Strobe in pin set to ' + str(value))

    def start_capture(self):
        if self.is_master:
            self.strobe_out_pin = 2
            self.strobe_in_pin = None
        else:
            self.strobe_out_pin = None
            self.strobe_in_pin = 2
        self._camera.startCapture()
        logger.trace('Started capture on camera {}'.format(self.serial_number))

    def stop_capture(self):
        self.strobe_in_pin = None
        self.strobe_out_pin = None
        self._camera.stopCapture()
        logger.trace('Stopped capture on camera {}'.format(self.serial_number))

    def get_image(self):
        return self._camera.retrieveBuffer()

    def disconnect(self):
        self._camera.disconnect()
        logger.success('Camera {} disconnected'.format(self.serial_number))

    def get_supported_modes_info(self):
        self.supported_modes = {}
        self.supported_modes_info = {}
        for mode_name, mode in pycap.MODE.__dict__.items():
            if mode_name[0] == 'M':
                format7_info, supported = self._camera.getFormat7Info(mode)
                if supported:
                    self.supported_modes.update({mode_name: mode})
                    self.supported_modes_info.update({mode_name: format7_info})
        for mode_name, info in self.supported_modes_info.items():
            logger.trace(mode_name)
            for attribute, value in info.__dict__.items():
                if attribute[0] != '_':
                    logger.trace('    {}: {}'.format(attribute, value))

    def reset(self):
        self._camera.setConfiguration(numBuffers=100,
                                      grabMode=pycap.GRAB_MODE.BUFFER_FRAMES,
                                      highPerformanceRetrieveBuffer=True,
                                      grabTimeout=1000)
        self.get_supported_modes_info()
        format7_image_setting, packet_size, packet_percent = \
            self._camera.getFormat7Configuration()
        self.max_width = self.supported_modes_info['MODE_0'].maxWidth
        self.max_height = self.supported_modes_info['MODE_0'].maxHeight
        self.x_step = self.supported_modes_info['MODE_0'].offsetHStepSize
        self.y_step = self.supported_modes_info['MODE_0'].offsetVStepSize
        self.width_step = self.supported_modes_info['MODE_0'].imageHStepSize
        self.height_step = self.supported_modes_info['MODE_0'].imageVStepSize
        format7_image_setting.mode = self.supported_modes['MODE_0']
        format7_image_setting.width = self.max_width
        format7_image_setting.height = self.max_height
        format7_image_setting.offsetX = 0
        format7_image_setting.offsetY = 0
        format7_image_setting.PixelFormat = pycap.PIXEL_FORMAT.RAW8
        packet_info, is_valid = \
            self._camera.validateFormat7Settings(format7_image_setting)
        if is_valid:
            packet_size = packet_info.recommendedBytesPerPacket
            self._camera.setFormat7ConfigurationPacket(packet_size,
                                                       format7_image_setting)
        self._camera.setEmbeddedImageInfo(timestamp=True,
                                          gain=False,
                                          shutter=False,
                                          brightness=False,
                                          exposure=False,
                                          whiteBalance=False,
                                          frameCounter=False,
                                          strobePattern=False,
                                          GPIOPinState=False,
                                          ROIPosition=False)
        self.brightness = 0
        self.exposure = 0
        self.gamma = 1
        self.shutter = 10
        self.gain = 0
        if self.is_master:
            self.frame_rate = 75
        else:
            self.frame_rate = 1
        self.strobe_out_pin = None
        self._strobe_in_pin = None
        logger.success('Camera {} has been reset'.format(self.serial_number))
