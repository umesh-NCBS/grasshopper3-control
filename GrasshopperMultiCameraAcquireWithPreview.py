#! /usr/bin/env python3

# Author: Umesh Mohan (moh@nume.sh)

from CONFIGURATION import CONFIGURATION, N_FRAMES, PREVIEW_EVERY, FPS,\
    TEENSY_PORT, CIRCULAR_BUFFER_N_FRAMES
from Grasshopper.Record import record
from Teensy.Teensy import Teensy
from loguru import logger
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
import multiprocessing as mp
from time import sleep

teensy = Teensy(TEENSY_PORT)
teensy.set_frequency(FPS)


class PreviewGUI(QtGui.QWidget):
    def __init__(self, cameras_configuration, n_frames, preview_every):
        logger.trace('Preview GUI initializing')
        QtGui.QWidget.__init__(self)
        self.setWindowTitle("Grasshopper multi camera acquisition")
        self.layout = QtGui.QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)
        self.showMaximized()
        self.plot_widget = pg.GraphicsLayoutWidget(border={'color': '600',
                                                           'width': 1.5})
        self.layout.addWidget(self.plot_widget)
        self.interrupt_event = mp.Event()
        self.stop_circular_write_event = mp.Event()
        self.plots = {}
        self.plot_images = {}
        self.t0s = {}
        self.preview_queue = mp.Queue()
        self.update_queue = mp.Queue()
        self.record_process = mp.Process(
            target=record,
            args=(cameras_configuration, n_frames),
            kwargs={'preview_nth_frame': preview_every,
                    'interrupt_event': self.interrupt_event,
                    'preview_queue': self.update_queue,
                    'circular_buffer_n_frames': CIRCULAR_BUFFER_N_FRAMES,
                    'stop_circular_write_event': self.stop_circular_write_event})
        self.record_process.start()
        logger.trace('Multi camera record process started on process {}'\
            .format(self.record_process.pid))
        self.update_timer = QtCore.QTimer()
        self.update_timer.timeout.connect(self.update)
        self.update_timer.start(30)

    def keyPressEvent(self, event):
        logger.trace('Keypress event from qt')
        if event.key() == QtCore.Qt.Key_Backspace:
            logger.trace('key is backspace. setting interrupt')
            self.interrupt_event.set()
            event.accept()
        elif event.key() == QtCore.Qt.Key_Space:
            logger.trace('key is space. setting stop_circular_write_event interrupt')
            self.stop_circular_write_event.set()
            event.accept()

    def add_camera(self, camera_serial_number):
        if len(self.plots) > 0 and len(self.plots) % 2 == 0:
            self.plot_widget.nextRow()
        self.plots.update({
            camera_serial_number: self.plot_widget.addPlot(
                title=camera_serial_number)})
        self.plots[camera_serial_number].setAspectLocked(lock=True, ratio=1)
        for axis_position in ['left', 'bottom', 'right', 'top']:
            self.plots[camera_serial_number].hideAxis(axis_position)
        self.plot_images.update({camera_serial_number: pg.ImageItem()})
        self.plots[camera_serial_number].addItem(
            self.plot_images[camera_serial_number])

    def set_camera_image(self, camera_serial_number, 
                         frame_number, time, image):
        self.plot_images[camera_serial_number].setImage(image.T)
        self.plots[camera_serial_number].setTitle('{}<br>{:0.3f} s; N = {}'\
            .format(camera_serial_number, time, frame_number))

    def update(self):
        while not self.update_queue.empty():
            [frame_number, image] = self.update_queue.get()
            logger.trace('Got an image in queue from camera {}'\
                .format(image['camera serial number']))
            camera_serial_number = image['camera serial number']
            if camera_serial_number not in self.plots:
                logger.trace('Camera not added in plot. Adding now.')
                self.add_camera(camera_serial_number)
            logger.trace('Setting camera image in plot')
            if camera_serial_number not in self.t0s:
                self.t0s.update({camera_serial_number: image['timestamp']})
            time = image['timestamp'] - self.t0s[camera_serial_number]
            self.set_camera_image(camera_serial_number, 
                                  frame_number, time, np.reshape(image['data'], image['dimension']))


if __name__ == '__main__':
    import sys
    logger.remove()
    logger.add(sys.stderr, level='WARNING')
    pg.mkQApp()
    pg.setConfigOptions(antialias=True)
    preview_gui = PreviewGUI(CONFIGURATION, N_FRAMES, PREVIEW_EVERY)
    preview_gui.show()
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        pg.QtGui.QApplication.instance().exec_()
