#include "remote.h"

RF24 radio(RF24_CE_PIN, RF24_CSN_PIN);
uint8_t addresses[][6] = {"0Lab6", "1Lab6"};

CRGB LEDs[N_LEDs];

byte state = INITIALIZING;
byte previous_state = INITIALIZING;

void send_to_receiver(byte to_send) {
  radio.stopListening();
  byte percent_space_left = 0x00;
  Serial.print("Sending: ");
  Serial.println(to_send);
  if (!radio.write(&to_send, 1)) {
    Serial.println("failed");
  } else {
    if (!radio.available()) {
      Serial.println("Received blank payload");
    } else {
      while (radio.available()) {
        radio.read(&percent_space_left, 1);
        Serial.print("Got response: ");
        Serial.println(percent_space_left);
      }
    }
  }
  if (percent_space_left < 30) {
    indicate_space(1);
  }
  if (percent_space_left < 20) {
    indicate_space(5);
  }
  if (percent_space_left < 10) {
    indicate_space(50);
  }
  radio.startListening();
}

void setup() {
  Serial.begin(115200);
  delay(3000);
  radio.begin();
  radio.setAutoAck(1);
  radio.enableAckPayload();
  radio.setRetries(0,15);
  radio.setPayloadSize(1);
  radio.openWritingPipe(addresses[0]);
  radio.openReadingPipe(1, addresses[1]);
  radio.startListening();
  radio.printDetails();  

  FastLED.addLeds<NEOPIXEL, LEDs_PIN>(LEDs, N_LEDs);
  pinMode(RECORD_PIN, INPUT_PULLUP);
  pinMode(CALIBRATE_PIN, INPUT_PULLUP);
  pinMode(LINEAR_RECORD_PIN, INPUT_PULLUP);
  pinMode(NEXT_RECORD_PIN, INPUT_PULLUP);
}

void loop() {
  switch(state) {
    case INITIALIZING:
      state = READY;
      indicate(0,0,1, 0,0,1, 0,0,1, 0,0,1);
      previous_state = INITIALIZING;
      send_to_receiver(0x01);
      break;
    
    case READY:
      if(RECORD_SWITCH_ON) {
        state = RECORDING_CIRCULAR;
      }
      if(CALIBRATE_SWITCH_ON) {
        state = CALIBRATING;
      }
      if (previous_state != READY) {
        indicate(0,0,0, 0,0,1, 0,0,0, 0,0,0);
        previous_state = READY;
        send_to_receiver(0x02);
      }
      break;
    
    case CALIBRATING:
      if(!CALIBRATE_SWITCH_ON) {
        state = READY;
      }
      if (previous_state != CALIBRATING) {
        indicate(0,0,0, 0,1,0, 0,0,0, 0,0,0);
        previous_state = CALIBRATING;
        send_to_receiver(0x03);
      }
      break;
    
    case RECORDING_CIRCULAR:
      if(LINEAR_RECORD_BUTTON_PRESSED) {
        state = RECORDING_LINEAR;
      }
      if(NEXT_RECORD_BUTTON_PRESSED) {
        state = RECORDING_CIRCULAR;
        send_to_receiver(0x06);
        indicate(1,0,0, 0,0,0, 0,1,0, 0,0,0);
        delay(1000);
        indicate(1,0,0, 0,0,0, 0,0,0, 0,0,0);
      }
      if(!RECORD_SWITCH_ON) {
        state = READY;
      }
      if (previous_state != RECORDING_CIRCULAR) {
        indicate(1,0,0, 0,0,0, 0,0,0, 0,0,0);
        previous_state = RECORDING_CIRCULAR;
        send_to_receiver(0x04);
      }
      break;
    
    case RECORDING_LINEAR:
      if(NEXT_RECORD_BUTTON_PRESSED) {
        state = RECORDING_CIRCULAR;
      }
      if(!RECORD_SWITCH_ON) {
        state = READY;
      }
      if (previous_state != RECORDING_LINEAR) {
        indicate(1,0,0, 1,0,0, 1,0,0, 0,0,0);
        previous_state = RECORDING_LINEAR;
        send_to_receiver(0x05);
      }
      break;
  }
}
