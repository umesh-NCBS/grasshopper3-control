#include "FastLED.h"
#include "RF24.h"

#define LEDs_PIN 23
#define N_LEDs 4

#define BUTTON_LEFT_PIN 22
#define SWITCH_II_PIN 21
#define SWITCH_I_PIN 20
#define BUTTON_RIGHT_PIN 19

#define LINEAR_RECORD_PIN BUTTON_LEFT_PIN
#define LINEAR_RECORD_BUTTON_PRESSED !digitalReadFast(LINEAR_RECORD_PIN)
#define NEXT_RECORD_PIN BUTTON_RIGHT_PIN
#define NEXT_RECORD_BUTTON_PRESSED !digitalReadFast(NEXT_RECORD_PIN)
#define RECORD_PIN SWITCH_II_PIN
#define RECORD_SWITCH_ON !digitalReadFast(RECORD_PIN)
#define CALIBRATE_PIN SWITCH_I_PIN
#define CALIBRATE_SWITCH_ON !digitalReadFast(CALIBRATE_PIN)

#define RF24_CSN_PIN 10
#define RF24_CE_PIN 9

#define LED_right LEDs[0]
#define LED_center LEDs[1]
#define LED_left LEDs[2]
#define LED_bottom LEDs[3]

volatile byte lr,lg,lb,cr,cg,cb,rr,rg,rb,br,bg,bb;

#define indicate(lr_,lg_,lb_,cr_,cg_,cb_,rr_,rg_,rb_,br_,bg_,bb_) \
  lr = lr_;\
  lg = lg_;\
  lb = lb_;\
  cr = cr_;\
  cg = cg_;\
  cb = cb_;\
  rr = rr_;\
  rg = rg_;\
  rb = rb_;\
  br = br_;\
  bg = bg_;\
  bb = bb_;\
  LED_left.setRGB(lr,lg,lb);\
  LED_center.setRGB(cr,cg,cb);\
  LED_right.setRGB(rr,rg,rb);\
  LED_bottom.setRGB(br,bg,bb);\
  FastLED.show();

#define indicate_space(space) \
  br = space;\
  LED_left.setRGB(lr,lg,lb);\
  LED_center.setRGB(cr,cg,cb);\
  LED_right.setRGB(rr,rg,rb);\
  LED_bottom.setRGB(br,bg,bb);\
  FastLED.show();

enum state_enum {
  INITIALIZING,
  READY,
  CALIBRATING,
  RECORDING_CIRCULAR,
  RECORDING_LINEAR
  };
