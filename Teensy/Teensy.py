# Author: Umesh Mohan (moh@nume.sh)

import serial
from loguru import logger
from time import sleep


class Teensy:
    def __init__(self, port):
        self._serial = serial.Serial(port=port, baudrate=115200, timeout=0.05)
        logger.trace(self._serial.readall())
        self._frequency = -1

    def set_frequency(self, frequency):
        if self._frequency == frequency:
            return
        assert(0 <= frequency <= 500)
        confirm_value = -1
        command = 'f={};'.format(frequency)
        logger.trace(self._serial.readall())
        while confirm_value != frequency:
            sleep(0.1)
            self._serial.write(command.encode())
            sleep(0.1)
            confirm_string = self._serial.readall().decode('UTF-8')
            logger.trace(confirm_string)
            if '\r\n' in confirm_string:
                confirm_value = int(confirm_string.split('\r')[0])
        logger.trace(confirm_value)
        self._frequency = confirm_value
