#define output_pin 23
#define on_duration 0.0015

int ts;
#define WaitTillSerialInputAvailableOrTimeout \
ts = millis();\
while (Serial.available() == 0) {\
  if (ts - millis() > SERtimeout) {\
    break;\
  }\
}

volatile uint16_t analog_value = 0;

#include <SPI.h>
#include "RF24.h"

#define RF24_CSN_PIN 10
#define RF24_CE_PIN 9

RF24 radio(RF24_CE_PIN, RF24_CSN_PIN);
uint8_t addresses[][6] = {"0Lab6", "1Lab6"};
byte percent_space_left=99;

void setup() {
  Serial.begin(115200);
  delay(5000);
  radio.begin();
  radio.setAutoAck(1);
  radio.enableAckPayload();
  radio.setRetries(0,15);
  radio.setPayloadSize(1);
  radio.openWritingPipe(addresses[1]);
  radio.openReadingPipe(1, addresses[0]);
  radio.startListening();
  radio.printDetails();
  analogWriteFrequency(output_pin, 75);
  analogWriteResolution(16);
}

void loop() {
  byte pipeNo;
  byte got;
  while (radio.available(&pipeNo)) {
    radio.read(&got, 1);
    Serial.print("Got: ");
    Serial.println(got);
    radio.writeAckPayload(pipeNo, &percent_space_left, 1);
    Serial.print("Sending back: ");
    Serial.print(percent_space_left);
    Serial.print(" on pipe: ");
    Serial.println(pipeNo);
    switch (got) {
      case 0x01: //INITIALIZING
        break;
      case 0x02: //READY
        break;
      case 0x03: //CALIBRATING
        Serial.println("Calibrating");
        break;
      case 0x04: //RECORDING_CIRCULAR
        Serial.println("Recording circular");
        break;
      case 0x05: //RECORDING_LINEAR
        Serial.println("Recording linear");
        Keyboard.print(" ");
        break;
      case 0x06: //NEXT_RECORD_BUTTON_PRESSED
        Serial.println("Next recording");
        Keyboard.print("N");
        break;
    }
  }
  if (Serial.available() > 0) {
    delay(10);
    String command_ = Serial.readString();
    int f_start = command_.indexOf("f=");
    int f_end = command_.indexOf(";");
    if (f_start != -1 && f_end != -1) {
      String f_string = command_.substring(f_start + 2, f_end);
      int trigger_frequency = f_string.toInt();
      Serial.println(trigger_frequency);
      analogWriteFrequency(output_pin, trigger_frequency);
      uint16_t analog_value = 0xffff - (0xffff * (trigger_frequency * on_duration));
      analogWrite(output_pin, analog_value);
      Serial.println(analog_value);
    }
    int s_start = command_.indexOf("s=");
    int s_end = command_.indexOf(";");
    if (s_start != -1 && s_end != -1) {
      String s_string = command_.substring(s_start + 2, s_end);
      percent_space_left = s_string.toInt();
      Serial.println(percent_space_left);
    }
  }
}
