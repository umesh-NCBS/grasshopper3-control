#define output_pin 23
#define stop_circular_buffer_pin 0
#define on_duration 0.0015

int ts;
#define WaitTillSerialInputAvailableOrTimeout \
ts = millis();\
while (Serial.available() == 0) {\
  if (ts - millis() > SERtimeout) {\
    break;\
  }\
}

volatile uint16_t analog_value = 0;

/*
uint16_t get_analog_value(float f, float on_duration) {
  return uint16_t (0xffff * (f * on_duration));
}
*/

void setup() {
  analogWriteFrequency(output_pin, 75);
  analogWriteResolution(16);
  pinMode(stop_circular_buffer_pin, INPUT_PULLUP);
  attachInterrupt(stop_circular_buffer_pin, press_keyboard_space, FALLING);
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0) {
    delay(100);
    String command_ = Serial.readString();
    int f_start = command_.indexOf("f=");
    int f_end = command_.indexOf(";");
    if (f_start != -1 && f_end != -1) {
      String f_string = command_.substring(f_start + 2, f_end);
      int trigger_frequency = f_string.toInt();
      Serial.println(trigger_frequency);
      analogWriteFrequency(output_pin, trigger_frequency);
      uint16_t analog_value = 0xffff - (0xffff * (trigger_frequency * on_duration));
      analogWrite(output_pin, analog_value);
      Serial.println(analog_value);
    }
  }
}

void press_keyboard_space() {
  Keyboard.print(" ");
}
